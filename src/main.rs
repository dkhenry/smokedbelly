#![feature(phase)]
#![feature(slicing_syntax)]

extern crate http;
extern crate url;
#[phase(plugin)] 
extern crate regex_macros;
extern crate regex; 


use http::client::RequestWriter;
use http::method::Get;
use http::status;
use url::Url;
use std::str;
use std::collections::HashSet;
use std::comm::channel; 

static BASE : &'static str = "http://en.wikipedia.org";
static INITIAL: &'static str = "/wiki/Kevin_Bacon"; 
static FINAL: &'static str = "/wiki/Miley_Cyrus";
static NTASKS: uint = 3; 

struct Path { 
    target: String,
    path: Vec<String>
}

impl Clone for Path { 
    fn clone(&self) -> Path { 
        Path { target: self.target.clone() , path: self.path.clone() }
    }
}

fn check_path( p: Path ) -> Vec<Path> { 
    let url = Url::parse( (BASE.into_string() + p.target)[] ).unwrap();
    let request: RequestWriter = RequestWriter::new(Get, url).unwrap(); 
    let mut response = match request.read_response() {
        Ok(response) => response,
        Err((_request,error)) => fail!(":-( {}", error)
    };
    let mut rvalue = Vec::new(); 
    if response.status == status::Ok { 
        let body = match response.read_to_end() {
            Ok(body) => body,
            Err(err) => fail!("Reading response failed: {}", err),
        };
        let re = regex!(r"(/wiki/[a-zA-Z_]*)");
        let text = str::from_utf8(body.as_slice()).expect("");
        for cap in re.captures_iter(text) {
            if cap.at(1) == FINAL { 
                let mut f = p.path.clone();
                f.push(p.target);
                println!("Found it: {} -> {}", cap.at(1),f);
                fail!(":-)"); 
            }
            println!("Adding {} from {} to the crawl list", cap.at(1), p.path);
            let mut a = Path { 
                target: String::from_str(cap.at(1)), 
                path: p.path.clone() 
            };
            a.path.push(p.target.clone());
            rvalue.push(a);            
        }
    }
    rvalue
} 

fn main() {
    println!("Kevin Baconator");

    let (feedback_tx, feedback_rx) = channel(); 

    let mut s = HashSet::new(); 
    s.insert(String::from_str(INITIAL));
    let workers = Vec::from_fn(NTASKS, | id | { 
        let task_tx = feedback_tx.clone();
        let (tx,rx) = channel(); 
        spawn(proc() { 
            loop { 
                let p: Path = rx.recv(); 
                println!("Sending feedback for task {}", id); 
                task_tx.send( check_path( p ) );
            }
        });
        tx 
    });
    let mut seed = check_path( Path { target: String::from_str(INITIAL), path: Vec::new() } );
    let mut worker = 0u;
    loop { 
        for p in seed.iter() { 
            if !s.contains( &p.target ) { 
                println!("Sending jobs to task {}",worker % NTASKS); 
                workers[worker % NTASKS].send(p.clone());
                s.insert(p.target.clone() );
                worker = worker + 1u ; 
            }
        }
        seed = feedback_rx.recv();
    }
}
